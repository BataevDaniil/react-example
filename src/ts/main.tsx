import React from 'react';
import {render} from 'react-dom';
import $ from 'jquery';

class Update extends React.Component<any, any> {
	static defaultProps = {
		name: 'name',
		update: true
	};
	constructor(props:any) {
		super(props);
	}

	public componentDidUpdate(prevProp:any, prevState:any) {
		console.log('componenDidUpdate Update');
		console.log('prevProp = ', prevProp);
		console.log('prevState = ', prevState);
	}

	public shouldComponentUpdate(nextProps:any, nextState:any) {
		console.log('shouldComponentUpdate Update');
		console.log('prevProp = ', nextProps);
		console.log('prevState = ', nextState);
		return nextProps.update;
	}

	public render() {
		return(
			<div>
				Update {this.props.name}
			</div>
		)
	}
}

//====================================================================================

class Comp extends React.Component<any, any> {
	static defaultProps = {
		name: 'stranger',
		update: true
	};

	constructor(props: any) {
		super(props);
		// console.log(props);
	}

	public componentWillMount() {
		console.log('componentWillMount Comp')
	}

	public componentDidMount() {
		console.log('componentDidMount Comp');
	}

	public componentWillUnmount() {
		console.log('componentWillUnmount Comp');
	}

	public render() {
		return (
			<div>
				Comp {this.props.name}
			</div>
		);
	}
}

//====================================================================================

class App extends React.Component<any, any> {
	public state = {
		visibleComp: true,
		name: 'name',
		update: true
	};

	constructor(props: any) {
		super(props);
	}

	public componentWillMount() {
		console.log('componentWillMount App')
	}

	public componentDidMount() {
		console.log('componentDidMount App');
	}

	public componentWillUnmount() {
		console.log('componentWillUnmount App');
	}

	public render() {
		return (
			<div>
				App
				<hr/>
				{this.state.visibleComp && <Comp/>}
				<div onClick={() => this.setState({
					visibleComp: !this.state.visibleComp,
				})}>
					button visible
				</div>
				<hr/>
				<Update name={this.state.name} update={this.state.update}/>
				<div onClick={() => this.setState({
					name: this.state.name === 'name'?'eman':'name'
				})}>
					button update
				</div>
				<div onClick={() => this.setState({
					update: !this.state.update,
				})}>
					button update?
				</div>
				<hr/>
			</div>
		);
	}
}

$(() => {
	render(<App/>, $('#react').get(0));
});


