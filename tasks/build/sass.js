const gulp = require('gulp');
const {reload}= require('browser-sync');

const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const removeComments = require('gulp-strip-css-comments');
const sourcemaps = require('gulp-sourcemaps');

const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

const path = require('../config');

gulp.task('sass-build', () => {
	return gulp.src(path.src.sass)
		.pipe(plumber({
			errorHandler: notify.onError(function(err){
			return {
				title: 'Error pug',
				message: err.message
			}})
		}))
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
		}))
		.pipe(removeComments())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(path.build.css))
		.pipe(reload({stream: true}));
});