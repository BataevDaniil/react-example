const gulp = require('gulp');
const {reload}= require('browser-sync');

const path = require('./tasks/config');

const requireDir = require('require-dir');
requireDir('./tasks', { recurse: true });

gulp.task('build',
	gulp.series('clean',
	gulp.parallel(
	              'haml-build',
	              'sass-build',
	              'ts-build')));

gulp.task('watch', () => {
	gulp.watch(path.watch.haml, gulp.series('haml-build'));
	gulp.watch(path.watch.sass, gulp.series('sass-build'));
});

gulp.task('default',
	gulp.series('build',
	gulp.parallel(
	              'img',
	              'browser-sync',
	              'watch')));

